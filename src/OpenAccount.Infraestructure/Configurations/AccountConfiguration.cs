﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OpenAccount.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Infra.Mappings
{
    public class AccountConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasKey(d => d.AccountId);

            builder.Property(d => d.Agency)
                .IsRequired()
                .HasMaxLength(4);

            builder.Property(d => d.Holder)
                .IsRequired();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Domain.Interfaces.Services;
using OpenAccount.Domain.Validators;
using OpenAccount.Infra.CrossCutting.IoC;

namespace Api.OpenAccount.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HolderController : ControllerBase
    {
        private readonly IHolderService _service;

        public HolderController()
        {
            _service = Resolver.Resolve<IHolderService>();
            
        }

        [HttpGet("{id}")]
        public ActionResult<Holder> Get(int id)
        {
            return _service.Get(id);
        }

        [HttpPost]
        public Holder Post([FromBody] Holder holder)
        {
            _service.Create<HolderValidator>(holder);

            return holder;
        }    
    }
}

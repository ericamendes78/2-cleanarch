﻿using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Infra.Context;
using System.Data.SqlClient;

namespace SOLID.DIP.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private ContextDB _context {get; set;}

        public AccountRepository(ContextDB context)
        {
            _context = context;
        }

        #region "Methods"
        public void Save(Account account)
        {
            _context.Accounts.Add(account);
             _context.SaveChanges();
        }

        public Account Get(long id)
        {
            Account result = _context.Accounts.Find(id);
            return result;
        }

        #endregion
    }
}

﻿using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Services;

namespace OpenAccount.Domain.Interfaces.Services
{
    public interface IHolderService : IRegisterService<Holder>
    {
    }
}

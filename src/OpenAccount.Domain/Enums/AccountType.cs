﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Domain.Enums
{
    public enum AccountType
    {
        CHECKING,
        INVESTIMENT,
        SAVINGS
    }
}

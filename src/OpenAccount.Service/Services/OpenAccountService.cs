﻿using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Domain.Interfaces.Services;
using OpenAccount.Domain.Validators;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Service.Services
{
    public class OpenAccountService : IOpenAccountService
    {
        private readonly IAccountService _accountService;
        private readonly IHolderService _holderService;

        public OpenAccountService(IAccountService accountService, IHolderService holderService)
        {
            _accountService = accountService;
            _holderService = holderService;
        }

        public void OpenAccount(Account account)
        {
            _holderService.Create<HolderValidator>(account.Holder);
            _accountService.Create<AccountValidator>(account);
        }
    }
}

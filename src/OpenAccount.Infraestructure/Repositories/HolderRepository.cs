﻿using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Infra.Context;
using System.Data.SqlClient;

namespace SOLID.DIP.Repositories
{
    public class HolderRepository : IHolderRepository
    {
        private ContextDB _context { get; set; }

        public HolderRepository(ContextDB context)
        {
            _context = context;
        }

        #region "Methods"
        public void Save(Holder holder)
        {
            _context.Holders.Add(holder);
            _context.SaveChanges();
        }

        public Holder Get(long id)
        {
            Holder result = _context.Holders.Find(id);
            return result;
        }

        #endregion
    }
}

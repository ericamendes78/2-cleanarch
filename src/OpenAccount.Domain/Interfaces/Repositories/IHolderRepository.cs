﻿using OpenAccount.Domain.Entities;

namespace OpenAccount.Domain.Interfaces.Repositories
{
    public interface IHolderRepository : IRepository<Holder>
    {

    }
}

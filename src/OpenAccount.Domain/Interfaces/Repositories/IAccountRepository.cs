﻿using OpenAccount.Domain.Entities;

namespace OpenAccount.Domain.Interfaces.Repositories
{
    public interface IAccountRepository : IRepository<Account>
    {
    }
}

﻿using OpenAccount.Domain.Entities;

namespace OpenAccount.Domain.Interfaces.Services
{
    public interface IOpenAccountService
    {
        void OpenAccount(Account account);
    }
}

﻿namespace OpenAccount.Domain.Interfaces.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Save(TEntity entity);
        TEntity Get(long id);

    }
}

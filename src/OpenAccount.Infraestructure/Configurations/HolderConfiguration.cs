﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OpenAccount.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Infra.Mappings
{
    public class HolderConfiguration : IEntityTypeConfiguration<Holder>
    {
        public void Configure(EntityTypeBuilder<Holder> builder)
        {
            builder
                .HasKey(d => d.CPF);

            builder.Property(d => d.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(d => d.BirthDate)
                .IsRequired();

            builder.Property(d => d.Email)
                .IsRequired()
                .HasMaxLength(30);

            builder.Property(d => d.Telephone)
                .IsRequired()
                .HasMaxLength(11);


        }
    }
}

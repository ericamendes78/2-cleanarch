﻿using FluentValidation;
using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Service.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _respository;

        public AccountService(IAccountRepository respository)
        {
            _respository = respository;
        }

        public void Create<TValidator>(Account account) where TValidator : AbstractValidator<Account>
        {
            Validate(account, Activator.CreateInstance<TValidator>());

            _respository.Save(account);
        }

        public Account Get(long id)
        {
            return _respository.Get(id);
        }
        public void Validate(Account account, AbstractValidator<Account> validator)
        {
            if (account == null)
            {
                throw new Exception("Invalid data for account registration.");
            }

            validator.ValidateAndThrow(account);
        }
    }
}

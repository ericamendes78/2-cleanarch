﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Domain.Validators;
using OpenAccount.Infra.CrossCutting.IoC;

namespace Api.OpenAccount.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpenAccountController : ControllerBase
    {
        private readonly IOpenAccountService _service;

        public OpenAccountController()
        {
            _service = Resolver.Resolve<IOpenAccountService>();
            
        }

        [HttpPost]
        public Account Post([FromBody] Account account)
        {
            _service.OpenAccount(account);

            return account;
        }    
    }
}

﻿using System;
using Castle.Windsor;
using Castle.Windsor.Installer;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Domain.Interfaces.Services;
using OpenAccount.Service.Services;
using SOLID.DIP.Repositories;

namespace OpenAccount.Infra.CrossCutting.IoC
{
    public class Resolver
    {
        static WindsorContainer Container { get; set; }

        static Resolver()
        {
            Container = new WindsorContainer();
            RegisterDependencies();
        }

        private static void RegisterDependencies()
        {
            RegisterSingleton<IHolderRepository, HolderRepository>();
            RegisterSingleton<IAccountRepository, AccountRepository>();

            RegisterSingleton<IHolderService, HolderService>();
            RegisterSingleton<IAccountService, AccountService>();

        }

        private static void RegisterSingleton<X, Y>() where X : class
                                                      where Y : X
        {
            Container.Register(Castle.MicroKernel.Registration.Component.For<X>().ImplementedBy<Y>().LifestyleSingleton());
        }



        public static T Resolve<T>() where T : class
        {
            return Container.Resolve<T>();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using OpenAccount.Domain.Entities;
using OpenAccount.Infra.Mappings;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Infra.Context
{
    public class ContextDB : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Holder> Holders { get; set; }

        public ContextDB(DbContextOptions<ContextDB> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=[SERVIDOR];Port=[PORTA];Database=modelo;Uid=[USUARIO];Pwd=[SENHA]");
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Holder>(new HolderConfiguration().Configure);
            modelBuilder.Entity<Account>(new AccountConfiguration().Configure);

        }
    }
}

﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Domain.Interfaces.Services
{
    public interface IRegisterService<TEntity> where TEntity : class
    {
        void Create<TValidator>(TEntity entity) where TValidator : AbstractValidator<TEntity>;

        TEntity Get(long id);

        void Validate(TEntity entity, AbstractValidator<TEntity> validator);

    }
}

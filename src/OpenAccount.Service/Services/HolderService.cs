﻿using FluentValidation;
using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Service.Services
{
    public class HolderService : IHolderService
    {
        private readonly IHolderRepository _respository;

        public HolderService(IHolderRepository respository)
        {
            _respository = respository;
        }

        public void Create<TValidator>(Holder holder) where TValidator : AbstractValidator<Holder>
        {
            Validate(holder, Activator.CreateInstance<TValidator>());

            _respository.Save(holder);
        }

        public Holder Get(long id)
        {
            return _respository.Get(id);
        }

        public void Validate(Holder holder, AbstractValidator<Holder> validator)
        {
            if (holder == null)
            {
                throw new Exception("Invalid data for customer registration.");
            }

            validator.ValidateAndThrow(holder);
        }
    }
}

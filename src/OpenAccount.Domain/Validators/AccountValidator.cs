﻿using FluentValidation;
using OpenAccount.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Domain.Validators
{
    public class AccountValidator : AbstractValidator<Account>
    {
        public AccountValidator()
        {
            RuleFor(c => c.Agency)
                .NotEmpty().WithMessage("Invalid agency.")
                .NotNull().WithMessage("Invalid agency.");

            RuleFor(c => c.Balance)
               .Must(x => x < 0).WithMessage("Insulfficient balance for opening the account.");

        }
    }
}


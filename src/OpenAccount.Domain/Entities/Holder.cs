﻿using OpenAccount.Domain.Validators;
using System;

namespace OpenAccount.Domain.Entities
{
    public class Holder
    {
        #region "Properties"
        public string Name { get; private set; }
        public string CPF { get; private set; }
        public DateTime BirthDate { get; private set; }
        public string Email { get; private set; }
        public long Telephone { get; private set; }

        #endregion

        #region "Constructors"

        public Holder(string Name, string CPF, DateTime BirthDate, string Email, long Telephone)
        {
            this.Name = Name;
            this.CPF = CPF;
            this.BirthDate = BirthDate;
            this.Email = Email;
            this.Telephone = Telephone;
        }

        #endregion 

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OpenAccount.Domain.Entities;
using OpenAccount.Domain.Interfaces.Repositories;
using OpenAccount.Domain.Validators;
using OpenAccount.Infra.CrossCutting.IoC;

namespace Api.OpenAccount.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _service;

        public AccountController()
        {
            _service = Resolver.Resolve<IAccountService>();
            
        }

        [HttpGet("{id}")]
        public ActionResult<Account> Get(int id)
        {
            return _service.Get(id);
        }

        [HttpPost]
        public Account Post([FromBody] Account account)
        {
            _service.Create<AccountValidator>(account);

            return account;
        }    
    }
}

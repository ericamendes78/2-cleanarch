﻿using FluentValidation;
using OpenAccount.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenAccount.Domain.Validators
{
    public class HolderValidator : AbstractValidator<Holder>
    {
        public HolderValidator()
        {

            RuleFor(c => c.Name)
                .NotEmpty().WithMessage("Invalid name.")
                .NotNull().WithMessage("Invalid name.");

            RuleFor(c => c.CPF)
                .NotEmpty().WithMessage("Invalid CPF.")
                .NotNull().WithMessage("Invalid CPF.")
                .Must(x => x.Length != 11).WithMessage("Invalid CPF.");

            RuleFor(c => c.BirthDate)
                .Must(x => x == DateTime.MinValue).WithMessage("Invalid birthday date.");

            RuleFor(c => c.Email)
                .Must(x => x.Length < 10).WithMessage("E-mail invalid to receive invoice.")
                .Must(x => !x.Contains("@")).WithMessage("E-mail invalid to receive invoice.");


            RuleFor(c => c.Telephone)
                .Must(x => x.ToString().Length < 10).WithMessage("Telephone invalid to receive invoice by SMS.");

        }
    }
}

